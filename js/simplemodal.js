var modal = document.getElementById('simplemodal');
var modalBtn = document.getElementById('modalbtn');
var closeBtn = document.getElementsByClassName('closebtn')[0];

modalBtn.addEventListener('click', openmodal);
closeBtn.addEventListener('click', closemodal);
window.addEventListener('click', clickoutside);

function openmodal(){
	/*console.log(simplemodal II Modal opened!)*/
	modal.style.display = 'block';

}

function closemodal(){
	/*console.log(simplemodal II Modal closed)*/
	modal.style.display = 'none';

}

function clickoutside(e){
	if(e.target == modal){
		/*console.log(simplemodal II Modal closed)*/
		modal.style.display = 'none';
	}
}