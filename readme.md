# simplemodal

Very very simple modal made only with vanilla JS, html and css.
If you want to use this repo for your project, feel free to use it, modify it or whatever you wish. Sharing is caring. :))


## Screenshots:

![sm1.png](https://i.imgur.com/Btsb47Y.png)
![sm2.png](https://i.imgur.com/5DQJyfj.png)